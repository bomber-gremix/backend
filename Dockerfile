FROM node:12.5.0-alpine as builder

WORKDIR /app
COPY package.json package-lock.json ./
RUN npm install

COPY . .
RUN npm run build

FROM node:12.5.0-alpine

WORKDIR /app
COPY package.json package-lock.json ./
RUN npm install --only=prod

COPY --from=builder /app/prod/ ./prod

EXPOSE 4000
CMD ["npm", "start"]

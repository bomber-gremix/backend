import http from 'http';
import { server as WSServer, request } from 'websocket';

import { Lobby } from './network/lobby';
import { Connection } from './network/connection';
import { TerrainCell } from './engine/terrain';

const mapDefaultJson: TerrainCell[][] = require('./assets/map.default.json');
export const lobbyList: { [key: string]: Lobby } = {
  testlobby: new Lobby(mapDefaultJson),
};

export const server = http.createServer((_, res) => {
  res.writeHead(404);
  res.end();
});
server.listen(8080, () => console.log('🚀  Server is listening on port 8080'));

const wsServer = new WSServer({
  httpServer: server,
  autoAcceptConnections: false,
  disableNagleAlgorithm: true,
});

function originIsAllowed(origin: string) {
  // TODO: Check request origin
  return true;
}

wsServer.on('request', (req: request) => {
  if (!originIsAllowed(req.origin)) {
    req.reject();
    return;
  }

  const connection = req.accept('game-protocol', req.origin);
  new Connection(connection);
});

import * as messages from './messages';
import { Connection } from './connection';

describe('Connection test', () => {
  it('Should send valid "start" event message', () => {
    const sendMock = jest.fn();
    const conn = new Connection({ on: () => {}, send: sendMock } as any);

    conn.send('start', null);
    const startMessage: Buffer = sendMock.mock.calls[0][0];

    expect(messages.Event.decode(startMessage)).toMatchObject({
      eventName: 'start',
    });
  });
  it('Should send valid "bombExplode" event message', () => {
    const sendMock = jest.fn();
    const conn = new Connection({ on: () => {}, send: sendMock } as any);

    conn.send('bombExplode', { x: 0, y: 0 });
    const bombExplodeMessage: Buffer = sendMock.mock.calls[0][0];

    expect(messages.Event.decode(bombExplodeMessage)).toMatchObject({
      eventName: 'bombExplode',
      position: { x: 0, y: 0 },
    });
  });
  it('Should send valid "destroyBox" event message', () => {
    const sendMock = jest.fn();
    const conn = new Connection({ on: () => {}, send: sendMock } as any);

    conn.send('destroyBox', {
      position: { x: 0, y: 0 },
      boostKind: 'bombCount',
    });
    const destroyBoxMessage: Buffer = sendMock.mock.calls[0][0];

    const destroyBoxEventMessage: messages.Event = messages.Event.decode(
      destroyBoxMessage,
    );
    expect(destroyBoxEventMessage.eventName).toBe('destroyBox');
    expect(destroyBoxEventMessage.destroyBoxEvent).toMatchObject({
      position: { x: 0, y: 0 },
      boostKind: 'bombCount',
    });
  });
  it('Should send valid "move" event message', () => {
    const sendMock = jest.fn();
    const conn = new Connection({ on: () => {}, send: sendMock } as any);

    conn.send('move', {
      direction: 'up',
      player: { id: '0', nickname: 'test' },
    } as any);
    const moveMessage: Buffer = sendMock.mock.calls[0][0];

    const moveEventMessage: messages.Event = messages.Event.decode(moveMessage);
    expect(moveEventMessage.eventName).toBe('move');
    expect(moveEventMessage.moveEvent).toMatchObject({
      direction: 'up',
      player: { id: '0', nickname: 'test' },
    });
  });
  it('Should send valid "spawn" event message', () => {
    const sendMock = jest.fn();
    const conn = new Connection({ on: () => {}, send: sendMock } as any);

    conn.send('spawn', {
      position: { x: 1, y: 1 },
      player: { id: '0', nickname: 'test' },
    } as any);
    const moveMessage: Buffer = sendMock.mock.calls[0][0];

    const moveEventMessage: messages.Event = messages.Event.decode(moveMessage);
    expect(moveEventMessage.eventName).toBe('spawn');
    expect(moveEventMessage.spawnEvent).toMatchObject({
      position: { x: 1, y: 1 },
      player: { id: '0', nickname: 'test' },
    });
  });
});

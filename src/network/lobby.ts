import { Engine } from '@/engine';
import { Player } from '@/engine/entities/player';
import { TerrainCell } from '@/engine/terrain';
import { EventName, EventPayload } from '@/engine/typedef';

import * as messages from './messages';
import { Connection } from './connection';

export interface ILobby {
  players: { [key: string]: Player };
  connections: { [key: string]: Connection };

  broadcast(eventName: EventName, payload: EventPayload): void;
}

export class Lobby implements ILobby {
  engine: Engine;
  players: { [key: string]: Player } = {};
  connections: { [key: string]: Connection } = {};

  public connectedPlayers = 0;
  static maxPlayers = 5;

  constructor(schema: TerrainCell[][]) {
    this.engine = new Engine(this, schema);
  }
  newConnection(connection: Connection) {
    Object.values(this.players).forEach((player: Player) => {
      connection.send('spawn', {
        player,
        position: player.position,
      });
    });

    const player = this.engine.spawnPlayer(connection);
    this.connections[connection.id] = connection;
    this.players[connection.id] = player;
    this.connectedPlayers += 1;

    this.broadcast('spawn', {
      player,
      position: player.position,
    });

    connection.eventMessageHandler = (message: messages.Event) =>
      this.handleMessage(player, message);

    if (this.connectedPlayers === Lobby.maxPlayers) {
      this.broadcast('start', null);
    }
  }

  handleMessage(player: Player, message: messages.Event) {
    switch (message.eventName) {
      case 'move':
        player.move(message.moveEvent!.direction as any);
        break;
    }
  }

  broadcast(eventName: EventName, payload: EventPayload): void {
    Object.values(this.connections).forEach((conn: Connection) => {
      conn.send(eventName, payload);
    });
  }
}

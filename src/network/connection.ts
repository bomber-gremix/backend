import crypto from 'crypto';
import * as ws from 'websocket';

import { lobbyList } from '@/index';

import * as engine from '@/engine/typedef';
import * as messages from './messages';
import { Lobby } from './lobby';

export interface IConnection {
  id: string;

  send(eventName: engine.EventName, payload: engine.EventPayload): void;
}

export class Connection implements IConnection {
  public id: string = crypto.randomBytes(8).toString('hex');
  public nickname?: string;
  private authorized = false;

  public eventMessageHandler?: (event: messages.Event) => void;

  constructor(private connection: ws.connection) {
    connection.on('message', this.handleRawMessage);
  }

  public send(eventName: engine.EventName, payload: engine.EventPayload): void {
    const eventMessage: Partial<messages.IEvent> = {};

    eventMessage.eventName = eventName;
    switch (eventName) {
      case 'bombExplode':
        eventMessage.position = payload as engine.Position;
        break;
      case 'putBomb':
        eventMessage.putBombEvent = messages.PubBombEvent.fromObject(
          payload as engine.PutBombEvent,
        );
        break;
      case 'destroyBox':
        eventMessage.destroyBoxEvent = messages.DestroyBoxEvent.fromObject(
          payload as engine.DestroyBoxEvent,
        );
        break;
      case 'move':
        eventMessage.moveEvent = messages.MoveEvent.fromObject(
          payload as engine.MoveEvent,
        );
        break;
      case 'spawn':
        const {
          player: { id, nickname },
          position,
        } = payload as engine.SpawnEvent;
        eventMessage.spawnEvent = messages.SpawnEvent.fromObject({
          position,
          player: { id, nickname },
        });
        break;
    }

    const messageBytes = messages.Event.encode(
      eventMessage as messages.IEvent,
    ).finish();
    this.connection.send(messageBytes);
  }
  public close() {
    this.connection.close();
    this.handleRawMessage = () => {};
  }

  private authorize = (data: any) => {
    let clientHello: messages.ClientHello;

    try {
      clientHello = messages.ClientHello.decode(data);
    } catch {
      return;
    }

    if (!clientHello.lobbyName || !clientHello.nickname) return;

    const lobby: Lobby | undefined = lobbyList[clientHello.lobbyName];
    if (!lobby || lobby.connectedPlayers === Lobby.maxPlayers) return;

    lobby.newConnection(this);
  }
  private handleRawMessage = (ev: ws.IMessage) => {
    if (!ev.binaryData) return;
    if (!this.authorized) {
      this.authorize(ev.binaryData);
      return;
    }

    let event: messages.Event;
    try {
      event = messages.Event.decode(ev.binaryData);
    } catch {
      return;
    }

    switch (event.eventName as engine.EventName) {
      case 'move':
        if (this.eventMessageHandler) this.eventMessageHandler(event);
        break;
    }
  }
}

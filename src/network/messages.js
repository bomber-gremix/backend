/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/
"use strict";

var $protobuf = require("protobufjs/minimal");

// Common aliases
var $Reader = $protobuf.Reader, $Writer = $protobuf.Writer, $util = $protobuf.util;

// Exported root namespace
var $root = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});

$root.Player = (function() {

    /**
     * Properties of a Player.
     * @exports IPlayer
     * @interface IPlayer
     * @property {string} id Player id
     * @property {string} nickname Player nickname
     */

    /**
     * Constructs a new Player.
     * @exports Player
     * @classdesc Represents a Player.
     * @implements IPlayer
     * @constructor
     * @param {IPlayer=} [properties] Properties to set
     */
    function Player(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    /**
     * Player id.
     * @member {string} id
     * @memberof Player
     * @instance
     */
    Player.prototype.id = "";

    /**
     * Player nickname.
     * @member {string} nickname
     * @memberof Player
     * @instance
     */
    Player.prototype.nickname = "";

    /**
     * Creates a new Player instance using the specified properties.
     * @function create
     * @memberof Player
     * @static
     * @param {IPlayer=} [properties] Properties to set
     * @returns {Player} Player instance
     */
    Player.create = function create(properties) {
        return new Player(properties);
    };

    /**
     * Encodes the specified Player message. Does not implicitly {@link Player.verify|verify} messages.
     * @function encode
     * @memberof Player
     * @static
     * @param {IPlayer} message Player message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    Player.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(/* id 1, wireType 2 =*/10).string(message.id);
        writer.uint32(/* id 2, wireType 2 =*/18).string(message.nickname);
        return writer;
    };

    /**
     * Encodes the specified Player message, length delimited. Does not implicitly {@link Player.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Player
     * @static
     * @param {IPlayer} message Player message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    Player.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a Player message from the specified reader or buffer.
     * @function decode
     * @memberof Player
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Player} Player
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    Player.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.Player();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.id = reader.string();
                break;
            case 2:
                message.nickname = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("id"))
            throw $util.ProtocolError("missing required 'id'", { instance: message });
        if (!message.hasOwnProperty("nickname"))
            throw $util.ProtocolError("missing required 'nickname'", { instance: message });
        return message;
    };

    /**
     * Decodes a Player message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Player
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Player} Player
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    Player.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a Player message.
     * @function verify
     * @memberof Player
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    Player.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        if (!$util.isString(message.id))
            return "id: string expected";
        if (!$util.isString(message.nickname))
            return "nickname: string expected";
        return null;
    };

    /**
     * Creates a Player message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Player
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Player} Player
     */
    Player.fromObject = function fromObject(object) {
        if (object instanceof $root.Player)
            return object;
        var message = new $root.Player();
        if (object.id != null)
            message.id = String(object.id);
        if (object.nickname != null)
            message.nickname = String(object.nickname);
        return message;
    };

    /**
     * Creates a plain object from a Player message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Player
     * @static
     * @param {Player} message Player
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    Player.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        var object = {};
        if (options.defaults) {
            object.id = "";
            object.nickname = "";
        }
        if (message.id != null && message.hasOwnProperty("id"))
            object.id = message.id;
        if (message.nickname != null && message.hasOwnProperty("nickname"))
            object.nickname = message.nickname;
        return object;
    };

    /**
     * Converts this Player to JSON.
     * @function toJSON
     * @memberof Player
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    Player.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return Player;
})();

$root.Position = (function() {

    /**
     * Properties of a Position.
     * @exports IPosition
     * @interface IPosition
     * @property {number} x Position x
     * @property {number} y Position y
     */

    /**
     * Constructs a new Position.
     * @exports Position
     * @classdesc Represents a Position.
     * @implements IPosition
     * @constructor
     * @param {IPosition=} [properties] Properties to set
     */
    function Position(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    /**
     * Position x.
     * @member {number} x
     * @memberof Position
     * @instance
     */
    Position.prototype.x = 0;

    /**
     * Position y.
     * @member {number} y
     * @memberof Position
     * @instance
     */
    Position.prototype.y = 0;

    /**
     * Creates a new Position instance using the specified properties.
     * @function create
     * @memberof Position
     * @static
     * @param {IPosition=} [properties] Properties to set
     * @returns {Position} Position instance
     */
    Position.create = function create(properties) {
        return new Position(properties);
    };

    /**
     * Encodes the specified Position message. Does not implicitly {@link Position.verify|verify} messages.
     * @function encode
     * @memberof Position
     * @static
     * @param {IPosition} message Position message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    Position.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(/* id 1, wireType 0 =*/8).uint32(message.x);
        writer.uint32(/* id 2, wireType 0 =*/16).uint32(message.y);
        return writer;
    };

    /**
     * Encodes the specified Position message, length delimited. Does not implicitly {@link Position.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Position
     * @static
     * @param {IPosition} message Position message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    Position.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a Position message from the specified reader or buffer.
     * @function decode
     * @memberof Position
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Position} Position
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    Position.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.Position();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.x = reader.uint32();
                break;
            case 2:
                message.y = reader.uint32();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("x"))
            throw $util.ProtocolError("missing required 'x'", { instance: message });
        if (!message.hasOwnProperty("y"))
            throw $util.ProtocolError("missing required 'y'", { instance: message });
        return message;
    };

    /**
     * Decodes a Position message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Position
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Position} Position
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    Position.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a Position message.
     * @function verify
     * @memberof Position
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    Position.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        if (!$util.isInteger(message.x))
            return "x: integer expected";
        if (!$util.isInteger(message.y))
            return "y: integer expected";
        return null;
    };

    /**
     * Creates a Position message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Position
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Position} Position
     */
    Position.fromObject = function fromObject(object) {
        if (object instanceof $root.Position)
            return object;
        var message = new $root.Position();
        if (object.x != null)
            message.x = object.x >>> 0;
        if (object.y != null)
            message.y = object.y >>> 0;
        return message;
    };

    /**
     * Creates a plain object from a Position message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Position
     * @static
     * @param {Position} message Position
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    Position.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        var object = {};
        if (options.defaults) {
            object.x = 0;
            object.y = 0;
        }
        if (message.x != null && message.hasOwnProperty("x"))
            object.x = message.x;
        if (message.y != null && message.hasOwnProperty("y"))
            object.y = message.y;
        return object;
    };

    /**
     * Converts this Position to JSON.
     * @function toJSON
     * @memberof Position
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    Position.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return Position;
})();

$root.MoveEvent = (function() {

    /**
     * Properties of a MoveEvent.
     * @exports IMoveEvent
     * @interface IMoveEvent
     * @property {IPlayer} player MoveEvent player
     * @property {string} direction MoveEvent direction
     */

    /**
     * Constructs a new MoveEvent.
     * @exports MoveEvent
     * @classdesc Represents a MoveEvent.
     * @implements IMoveEvent
     * @constructor
     * @param {IMoveEvent=} [properties] Properties to set
     */
    function MoveEvent(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    /**
     * MoveEvent player.
     * @member {IPlayer} player
     * @memberof MoveEvent
     * @instance
     */
    MoveEvent.prototype.player = null;

    /**
     * MoveEvent direction.
     * @member {string} direction
     * @memberof MoveEvent
     * @instance
     */
    MoveEvent.prototype.direction = "";

    /**
     * Creates a new MoveEvent instance using the specified properties.
     * @function create
     * @memberof MoveEvent
     * @static
     * @param {IMoveEvent=} [properties] Properties to set
     * @returns {MoveEvent} MoveEvent instance
     */
    MoveEvent.create = function create(properties) {
        return new MoveEvent(properties);
    };

    /**
     * Encodes the specified MoveEvent message. Does not implicitly {@link MoveEvent.verify|verify} messages.
     * @function encode
     * @memberof MoveEvent
     * @static
     * @param {IMoveEvent} message MoveEvent message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    MoveEvent.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        $root.Player.encode(message.player, writer.uint32(/* id 1, wireType 2 =*/10).fork()).ldelim();
        writer.uint32(/* id 2, wireType 2 =*/18).string(message.direction);
        return writer;
    };

    /**
     * Encodes the specified MoveEvent message, length delimited. Does not implicitly {@link MoveEvent.verify|verify} messages.
     * @function encodeDelimited
     * @memberof MoveEvent
     * @static
     * @param {IMoveEvent} message MoveEvent message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    MoveEvent.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a MoveEvent message from the specified reader or buffer.
     * @function decode
     * @memberof MoveEvent
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {MoveEvent} MoveEvent
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    MoveEvent.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.MoveEvent();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.player = $root.Player.decode(reader, reader.uint32());
                break;
            case 2:
                message.direction = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("player"))
            throw $util.ProtocolError("missing required 'player'", { instance: message });
        if (!message.hasOwnProperty("direction"))
            throw $util.ProtocolError("missing required 'direction'", { instance: message });
        return message;
    };

    /**
     * Decodes a MoveEvent message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof MoveEvent
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {MoveEvent} MoveEvent
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    MoveEvent.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a MoveEvent message.
     * @function verify
     * @memberof MoveEvent
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    MoveEvent.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        {
            var error = $root.Player.verify(message.player);
            if (error)
                return "player." + error;
        }
        if (!$util.isString(message.direction))
            return "direction: string expected";
        return null;
    };

    /**
     * Creates a MoveEvent message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof MoveEvent
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {MoveEvent} MoveEvent
     */
    MoveEvent.fromObject = function fromObject(object) {
        if (object instanceof $root.MoveEvent)
            return object;
        var message = new $root.MoveEvent();
        if (object.player != null) {
            if (typeof object.player !== "object")
                throw TypeError(".MoveEvent.player: object expected");
            message.player = $root.Player.fromObject(object.player);
        }
        if (object.direction != null)
            message.direction = String(object.direction);
        return message;
    };

    /**
     * Creates a plain object from a MoveEvent message. Also converts values to other types if specified.
     * @function toObject
     * @memberof MoveEvent
     * @static
     * @param {MoveEvent} message MoveEvent
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    MoveEvent.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        var object = {};
        if (options.defaults) {
            object.player = null;
            object.direction = "";
        }
        if (message.player != null && message.hasOwnProperty("player"))
            object.player = $root.Player.toObject(message.player, options);
        if (message.direction != null && message.hasOwnProperty("direction"))
            object.direction = message.direction;
        return object;
    };

    /**
     * Converts this MoveEvent to JSON.
     * @function toJSON
     * @memberof MoveEvent
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    MoveEvent.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return MoveEvent;
})();

$root.SpawnEvent = (function() {

    /**
     * Properties of a SpawnEvent.
     * @exports ISpawnEvent
     * @interface ISpawnEvent
     * @property {IPlayer} player SpawnEvent player
     * @property {IPosition} position SpawnEvent position
     */

    /**
     * Constructs a new SpawnEvent.
     * @exports SpawnEvent
     * @classdesc Represents a SpawnEvent.
     * @implements ISpawnEvent
     * @constructor
     * @param {ISpawnEvent=} [properties] Properties to set
     */
    function SpawnEvent(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    /**
     * SpawnEvent player.
     * @member {IPlayer} player
     * @memberof SpawnEvent
     * @instance
     */
    SpawnEvent.prototype.player = null;

    /**
     * SpawnEvent position.
     * @member {IPosition} position
     * @memberof SpawnEvent
     * @instance
     */
    SpawnEvent.prototype.position = null;

    /**
     * Creates a new SpawnEvent instance using the specified properties.
     * @function create
     * @memberof SpawnEvent
     * @static
     * @param {ISpawnEvent=} [properties] Properties to set
     * @returns {SpawnEvent} SpawnEvent instance
     */
    SpawnEvent.create = function create(properties) {
        return new SpawnEvent(properties);
    };

    /**
     * Encodes the specified SpawnEvent message. Does not implicitly {@link SpawnEvent.verify|verify} messages.
     * @function encode
     * @memberof SpawnEvent
     * @static
     * @param {ISpawnEvent} message SpawnEvent message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    SpawnEvent.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        $root.Player.encode(message.player, writer.uint32(/* id 1, wireType 2 =*/10).fork()).ldelim();
        $root.Position.encode(message.position, writer.uint32(/* id 2, wireType 2 =*/18).fork()).ldelim();
        return writer;
    };

    /**
     * Encodes the specified SpawnEvent message, length delimited. Does not implicitly {@link SpawnEvent.verify|verify} messages.
     * @function encodeDelimited
     * @memberof SpawnEvent
     * @static
     * @param {ISpawnEvent} message SpawnEvent message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    SpawnEvent.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a SpawnEvent message from the specified reader or buffer.
     * @function decode
     * @memberof SpawnEvent
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {SpawnEvent} SpawnEvent
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    SpawnEvent.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.SpawnEvent();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.player = $root.Player.decode(reader, reader.uint32());
                break;
            case 2:
                message.position = $root.Position.decode(reader, reader.uint32());
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("player"))
            throw $util.ProtocolError("missing required 'player'", { instance: message });
        if (!message.hasOwnProperty("position"))
            throw $util.ProtocolError("missing required 'position'", { instance: message });
        return message;
    };

    /**
     * Decodes a SpawnEvent message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof SpawnEvent
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {SpawnEvent} SpawnEvent
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    SpawnEvent.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a SpawnEvent message.
     * @function verify
     * @memberof SpawnEvent
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    SpawnEvent.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        {
            var error = $root.Player.verify(message.player);
            if (error)
                return "player." + error;
        }
        {
            var error = $root.Position.verify(message.position);
            if (error)
                return "position." + error;
        }
        return null;
    };

    /**
     * Creates a SpawnEvent message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof SpawnEvent
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {SpawnEvent} SpawnEvent
     */
    SpawnEvent.fromObject = function fromObject(object) {
        if (object instanceof $root.SpawnEvent)
            return object;
        var message = new $root.SpawnEvent();
        if (object.player != null) {
            if (typeof object.player !== "object")
                throw TypeError(".SpawnEvent.player: object expected");
            message.player = $root.Player.fromObject(object.player);
        }
        if (object.position != null) {
            if (typeof object.position !== "object")
                throw TypeError(".SpawnEvent.position: object expected");
            message.position = $root.Position.fromObject(object.position);
        }
        return message;
    };

    /**
     * Creates a plain object from a SpawnEvent message. Also converts values to other types if specified.
     * @function toObject
     * @memberof SpawnEvent
     * @static
     * @param {SpawnEvent} message SpawnEvent
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    SpawnEvent.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        var object = {};
        if (options.defaults) {
            object.player = null;
            object.position = null;
        }
        if (message.player != null && message.hasOwnProperty("player"))
            object.player = $root.Player.toObject(message.player, options);
        if (message.position != null && message.hasOwnProperty("position"))
            object.position = $root.Position.toObject(message.position, options);
        return object;
    };

    /**
     * Converts this SpawnEvent to JSON.
     * @function toJSON
     * @memberof SpawnEvent
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    SpawnEvent.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return SpawnEvent;
})();

$root.PubBombEvent = (function() {

    /**
     * Properties of a PubBombEvent.
     * @exports IPubBombEvent
     * @interface IPubBombEvent
     * @property {IPlayer} player PubBombEvent player
     * @property {IPosition} position PubBombEvent position
     */

    /**
     * Constructs a new PubBombEvent.
     * @exports PubBombEvent
     * @classdesc Represents a PubBombEvent.
     * @implements IPubBombEvent
     * @constructor
     * @param {IPubBombEvent=} [properties] Properties to set
     */
    function PubBombEvent(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    /**
     * PubBombEvent player.
     * @member {IPlayer} player
     * @memberof PubBombEvent
     * @instance
     */
    PubBombEvent.prototype.player = null;

    /**
     * PubBombEvent position.
     * @member {IPosition} position
     * @memberof PubBombEvent
     * @instance
     */
    PubBombEvent.prototype.position = null;

    /**
     * Creates a new PubBombEvent instance using the specified properties.
     * @function create
     * @memberof PubBombEvent
     * @static
     * @param {IPubBombEvent=} [properties] Properties to set
     * @returns {PubBombEvent} PubBombEvent instance
     */
    PubBombEvent.create = function create(properties) {
        return new PubBombEvent(properties);
    };

    /**
     * Encodes the specified PubBombEvent message. Does not implicitly {@link PubBombEvent.verify|verify} messages.
     * @function encode
     * @memberof PubBombEvent
     * @static
     * @param {IPubBombEvent} message PubBombEvent message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    PubBombEvent.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        $root.Player.encode(message.player, writer.uint32(/* id 1, wireType 2 =*/10).fork()).ldelim();
        $root.Position.encode(message.position, writer.uint32(/* id 2, wireType 2 =*/18).fork()).ldelim();
        return writer;
    };

    /**
     * Encodes the specified PubBombEvent message, length delimited. Does not implicitly {@link PubBombEvent.verify|verify} messages.
     * @function encodeDelimited
     * @memberof PubBombEvent
     * @static
     * @param {IPubBombEvent} message PubBombEvent message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    PubBombEvent.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a PubBombEvent message from the specified reader or buffer.
     * @function decode
     * @memberof PubBombEvent
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {PubBombEvent} PubBombEvent
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    PubBombEvent.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.PubBombEvent();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.player = $root.Player.decode(reader, reader.uint32());
                break;
            case 2:
                message.position = $root.Position.decode(reader, reader.uint32());
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("player"))
            throw $util.ProtocolError("missing required 'player'", { instance: message });
        if (!message.hasOwnProperty("position"))
            throw $util.ProtocolError("missing required 'position'", { instance: message });
        return message;
    };

    /**
     * Decodes a PubBombEvent message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof PubBombEvent
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {PubBombEvent} PubBombEvent
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    PubBombEvent.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a PubBombEvent message.
     * @function verify
     * @memberof PubBombEvent
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    PubBombEvent.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        {
            var error = $root.Player.verify(message.player);
            if (error)
                return "player." + error;
        }
        {
            var error = $root.Position.verify(message.position);
            if (error)
                return "position." + error;
        }
        return null;
    };

    /**
     * Creates a PubBombEvent message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof PubBombEvent
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {PubBombEvent} PubBombEvent
     */
    PubBombEvent.fromObject = function fromObject(object) {
        if (object instanceof $root.PubBombEvent)
            return object;
        var message = new $root.PubBombEvent();
        if (object.player != null) {
            if (typeof object.player !== "object")
                throw TypeError(".PubBombEvent.player: object expected");
            message.player = $root.Player.fromObject(object.player);
        }
        if (object.position != null) {
            if (typeof object.position !== "object")
                throw TypeError(".PubBombEvent.position: object expected");
            message.position = $root.Position.fromObject(object.position);
        }
        return message;
    };

    /**
     * Creates a plain object from a PubBombEvent message. Also converts values to other types if specified.
     * @function toObject
     * @memberof PubBombEvent
     * @static
     * @param {PubBombEvent} message PubBombEvent
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    PubBombEvent.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        var object = {};
        if (options.defaults) {
            object.player = null;
            object.position = null;
        }
        if (message.player != null && message.hasOwnProperty("player"))
            object.player = $root.Player.toObject(message.player, options);
        if (message.position != null && message.hasOwnProperty("position"))
            object.position = $root.Position.toObject(message.position, options);
        return object;
    };

    /**
     * Converts this PubBombEvent to JSON.
     * @function toJSON
     * @memberof PubBombEvent
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    PubBombEvent.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return PubBombEvent;
})();

$root.DestroyBoxEvent = (function() {

    /**
     * Properties of a DestroyBoxEvent.
     * @exports IDestroyBoxEvent
     * @interface IDestroyBoxEvent
     * @property {IPosition} position DestroyBoxEvent position
     * @property {string|null} [boostKind] DestroyBoxEvent boostKind
     */

    /**
     * Constructs a new DestroyBoxEvent.
     * @exports DestroyBoxEvent
     * @classdesc Represents a DestroyBoxEvent.
     * @implements IDestroyBoxEvent
     * @constructor
     * @param {IDestroyBoxEvent=} [properties] Properties to set
     */
    function DestroyBoxEvent(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    /**
     * DestroyBoxEvent position.
     * @member {IPosition} position
     * @memberof DestroyBoxEvent
     * @instance
     */
    DestroyBoxEvent.prototype.position = null;

    /**
     * DestroyBoxEvent boostKind.
     * @member {string} boostKind
     * @memberof DestroyBoxEvent
     * @instance
     */
    DestroyBoxEvent.prototype.boostKind = "";

    /**
     * Creates a new DestroyBoxEvent instance using the specified properties.
     * @function create
     * @memberof DestroyBoxEvent
     * @static
     * @param {IDestroyBoxEvent=} [properties] Properties to set
     * @returns {DestroyBoxEvent} DestroyBoxEvent instance
     */
    DestroyBoxEvent.create = function create(properties) {
        return new DestroyBoxEvent(properties);
    };

    /**
     * Encodes the specified DestroyBoxEvent message. Does not implicitly {@link DestroyBoxEvent.verify|verify} messages.
     * @function encode
     * @memberof DestroyBoxEvent
     * @static
     * @param {IDestroyBoxEvent} message DestroyBoxEvent message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    DestroyBoxEvent.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        $root.Position.encode(message.position, writer.uint32(/* id 1, wireType 2 =*/10).fork()).ldelim();
        if (message.boostKind != null && message.hasOwnProperty("boostKind"))
            writer.uint32(/* id 2, wireType 2 =*/18).string(message.boostKind);
        return writer;
    };

    /**
     * Encodes the specified DestroyBoxEvent message, length delimited. Does not implicitly {@link DestroyBoxEvent.verify|verify} messages.
     * @function encodeDelimited
     * @memberof DestroyBoxEvent
     * @static
     * @param {IDestroyBoxEvent} message DestroyBoxEvent message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    DestroyBoxEvent.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a DestroyBoxEvent message from the specified reader or buffer.
     * @function decode
     * @memberof DestroyBoxEvent
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {DestroyBoxEvent} DestroyBoxEvent
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    DestroyBoxEvent.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.DestroyBoxEvent();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.position = $root.Position.decode(reader, reader.uint32());
                break;
            case 2:
                message.boostKind = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("position"))
            throw $util.ProtocolError("missing required 'position'", { instance: message });
        return message;
    };

    /**
     * Decodes a DestroyBoxEvent message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof DestroyBoxEvent
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {DestroyBoxEvent} DestroyBoxEvent
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    DestroyBoxEvent.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a DestroyBoxEvent message.
     * @function verify
     * @memberof DestroyBoxEvent
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    DestroyBoxEvent.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        {
            var error = $root.Position.verify(message.position);
            if (error)
                return "position." + error;
        }
        if (message.boostKind != null && message.hasOwnProperty("boostKind"))
            if (!$util.isString(message.boostKind))
                return "boostKind: string expected";
        return null;
    };

    /**
     * Creates a DestroyBoxEvent message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof DestroyBoxEvent
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {DestroyBoxEvent} DestroyBoxEvent
     */
    DestroyBoxEvent.fromObject = function fromObject(object) {
        if (object instanceof $root.DestroyBoxEvent)
            return object;
        var message = new $root.DestroyBoxEvent();
        if (object.position != null) {
            if (typeof object.position !== "object")
                throw TypeError(".DestroyBoxEvent.position: object expected");
            message.position = $root.Position.fromObject(object.position);
        }
        if (object.boostKind != null)
            message.boostKind = String(object.boostKind);
        return message;
    };

    /**
     * Creates a plain object from a DestroyBoxEvent message. Also converts values to other types if specified.
     * @function toObject
     * @memberof DestroyBoxEvent
     * @static
     * @param {DestroyBoxEvent} message DestroyBoxEvent
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    DestroyBoxEvent.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        var object = {};
        if (options.defaults) {
            object.position = null;
            object.boostKind = "";
        }
        if (message.position != null && message.hasOwnProperty("position"))
            object.position = $root.Position.toObject(message.position, options);
        if (message.boostKind != null && message.hasOwnProperty("boostKind"))
            object.boostKind = message.boostKind;
        return object;
    };

    /**
     * Converts this DestroyBoxEvent to JSON.
     * @function toJSON
     * @memberof DestroyBoxEvent
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    DestroyBoxEvent.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return DestroyBoxEvent;
})();

$root.ClientHello = (function() {

    /**
     * Properties of a ClientHello.
     * @exports IClientHello
     * @interface IClientHello
     * @property {string} nickname ClientHello nickname
     * @property {string} lobbyName ClientHello lobbyName
     */

    /**
     * Constructs a new ClientHello.
     * @exports ClientHello
     * @classdesc Represents a ClientHello.
     * @implements IClientHello
     * @constructor
     * @param {IClientHello=} [properties] Properties to set
     */
    function ClientHello(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    /**
     * ClientHello nickname.
     * @member {string} nickname
     * @memberof ClientHello
     * @instance
     */
    ClientHello.prototype.nickname = "";

    /**
     * ClientHello lobbyName.
     * @member {string} lobbyName
     * @memberof ClientHello
     * @instance
     */
    ClientHello.prototype.lobbyName = "";

    /**
     * Creates a new ClientHello instance using the specified properties.
     * @function create
     * @memberof ClientHello
     * @static
     * @param {IClientHello=} [properties] Properties to set
     * @returns {ClientHello} ClientHello instance
     */
    ClientHello.create = function create(properties) {
        return new ClientHello(properties);
    };

    /**
     * Encodes the specified ClientHello message. Does not implicitly {@link ClientHello.verify|verify} messages.
     * @function encode
     * @memberof ClientHello
     * @static
     * @param {IClientHello} message ClientHello message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    ClientHello.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(/* id 1, wireType 2 =*/10).string(message.nickname);
        writer.uint32(/* id 2, wireType 2 =*/18).string(message.lobbyName);
        return writer;
    };

    /**
     * Encodes the specified ClientHello message, length delimited. Does not implicitly {@link ClientHello.verify|verify} messages.
     * @function encodeDelimited
     * @memberof ClientHello
     * @static
     * @param {IClientHello} message ClientHello message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    ClientHello.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a ClientHello message from the specified reader or buffer.
     * @function decode
     * @memberof ClientHello
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {ClientHello} ClientHello
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    ClientHello.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.ClientHello();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.nickname = reader.string();
                break;
            case 2:
                message.lobbyName = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("nickname"))
            throw $util.ProtocolError("missing required 'nickname'", { instance: message });
        if (!message.hasOwnProperty("lobbyName"))
            throw $util.ProtocolError("missing required 'lobbyName'", { instance: message });
        return message;
    };

    /**
     * Decodes a ClientHello message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof ClientHello
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {ClientHello} ClientHello
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    ClientHello.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a ClientHello message.
     * @function verify
     * @memberof ClientHello
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    ClientHello.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        if (!$util.isString(message.nickname))
            return "nickname: string expected";
        if (!$util.isString(message.lobbyName))
            return "lobbyName: string expected";
        return null;
    };

    /**
     * Creates a ClientHello message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof ClientHello
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {ClientHello} ClientHello
     */
    ClientHello.fromObject = function fromObject(object) {
        if (object instanceof $root.ClientHello)
            return object;
        var message = new $root.ClientHello();
        if (object.nickname != null)
            message.nickname = String(object.nickname);
        if (object.lobbyName != null)
            message.lobbyName = String(object.lobbyName);
        return message;
    };

    /**
     * Creates a plain object from a ClientHello message. Also converts values to other types if specified.
     * @function toObject
     * @memberof ClientHello
     * @static
     * @param {ClientHello} message ClientHello
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    ClientHello.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        var object = {};
        if (options.defaults) {
            object.nickname = "";
            object.lobbyName = "";
        }
        if (message.nickname != null && message.hasOwnProperty("nickname"))
            object.nickname = message.nickname;
        if (message.lobbyName != null && message.hasOwnProperty("lobbyName"))
            object.lobbyName = message.lobbyName;
        return object;
    };

    /**
     * Converts this ClientHello to JSON.
     * @function toJSON
     * @memberof ClientHello
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    ClientHello.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return ClientHello;
})();

$root.ServerHello = (function() {

    /**
     * Properties of a ServerHello.
     * @exports IServerHello
     * @interface IServerHello
     * @property {string} id ServerHello id
     */

    /**
     * Constructs a new ServerHello.
     * @exports ServerHello
     * @classdesc Represents a ServerHello.
     * @implements IServerHello
     * @constructor
     * @param {IServerHello=} [properties] Properties to set
     */
    function ServerHello(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    /**
     * ServerHello id.
     * @member {string} id
     * @memberof ServerHello
     * @instance
     */
    ServerHello.prototype.id = "";

    /**
     * Creates a new ServerHello instance using the specified properties.
     * @function create
     * @memberof ServerHello
     * @static
     * @param {IServerHello=} [properties] Properties to set
     * @returns {ServerHello} ServerHello instance
     */
    ServerHello.create = function create(properties) {
        return new ServerHello(properties);
    };

    /**
     * Encodes the specified ServerHello message. Does not implicitly {@link ServerHello.verify|verify} messages.
     * @function encode
     * @memberof ServerHello
     * @static
     * @param {IServerHello} message ServerHello message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    ServerHello.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(/* id 1, wireType 2 =*/10).string(message.id);
        return writer;
    };

    /**
     * Encodes the specified ServerHello message, length delimited. Does not implicitly {@link ServerHello.verify|verify} messages.
     * @function encodeDelimited
     * @memberof ServerHello
     * @static
     * @param {IServerHello} message ServerHello message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    ServerHello.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes a ServerHello message from the specified reader or buffer.
     * @function decode
     * @memberof ServerHello
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {ServerHello} ServerHello
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    ServerHello.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.ServerHello();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.id = reader.string();
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("id"))
            throw $util.ProtocolError("missing required 'id'", { instance: message });
        return message;
    };

    /**
     * Decodes a ServerHello message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof ServerHello
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {ServerHello} ServerHello
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    ServerHello.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies a ServerHello message.
     * @function verify
     * @memberof ServerHello
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    ServerHello.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        if (!$util.isString(message.id))
            return "id: string expected";
        return null;
    };

    /**
     * Creates a ServerHello message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof ServerHello
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {ServerHello} ServerHello
     */
    ServerHello.fromObject = function fromObject(object) {
        if (object instanceof $root.ServerHello)
            return object;
        var message = new $root.ServerHello();
        if (object.id != null)
            message.id = String(object.id);
        return message;
    };

    /**
     * Creates a plain object from a ServerHello message. Also converts values to other types if specified.
     * @function toObject
     * @memberof ServerHello
     * @static
     * @param {ServerHello} message ServerHello
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    ServerHello.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        var object = {};
        if (options.defaults)
            object.id = "";
        if (message.id != null && message.hasOwnProperty("id"))
            object.id = message.id;
        return object;
    };

    /**
     * Converts this ServerHello to JSON.
     * @function toJSON
     * @memberof ServerHello
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    ServerHello.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return ServerHello;
})();

$root.Event = (function() {

    /**
     * Properties of an Event.
     * @exports IEvent
     * @interface IEvent
     * @property {string} eventName Event eventName
     * @property {IPosition|null} [position] Event position
     * @property {IMoveEvent|null} [moveEvent] Event moveEvent
     * @property {ISpawnEvent|null} [spawnEvent] Event spawnEvent
     * @property {IPubBombEvent|null} [putBombEvent] Event putBombEvent
     * @property {IDestroyBoxEvent|null} [destroyBoxEvent] Event destroyBoxEvent
     */

    /**
     * Constructs a new Event.
     * @exports Event
     * @classdesc Represents an Event.
     * @implements IEvent
     * @constructor
     * @param {IEvent=} [properties] Properties to set
     */
    function Event(properties) {
        if (properties)
            for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                if (properties[keys[i]] != null)
                    this[keys[i]] = properties[keys[i]];
    }

    /**
     * Event eventName.
     * @member {string} eventName
     * @memberof Event
     * @instance
     */
    Event.prototype.eventName = "";

    /**
     * Event position.
     * @member {IPosition|null|undefined} position
     * @memberof Event
     * @instance
     */
    Event.prototype.position = null;

    /**
     * Event moveEvent.
     * @member {IMoveEvent|null|undefined} moveEvent
     * @memberof Event
     * @instance
     */
    Event.prototype.moveEvent = null;

    /**
     * Event spawnEvent.
     * @member {ISpawnEvent|null|undefined} spawnEvent
     * @memberof Event
     * @instance
     */
    Event.prototype.spawnEvent = null;

    /**
     * Event putBombEvent.
     * @member {IPubBombEvent|null|undefined} putBombEvent
     * @memberof Event
     * @instance
     */
    Event.prototype.putBombEvent = null;

    /**
     * Event destroyBoxEvent.
     * @member {IDestroyBoxEvent|null|undefined} destroyBoxEvent
     * @memberof Event
     * @instance
     */
    Event.prototype.destroyBoxEvent = null;

    // OneOf field names bound to virtual getters and setters
    var $oneOfFields;

    /**
     * Event payload.
     * @member {"position"|"moveEvent"|"spawnEvent"|"putBombEvent"|"destroyBoxEvent"|undefined} payload
     * @memberof Event
     * @instance
     */
    Object.defineProperty(Event.prototype, "payload", {
        get: $util.oneOfGetter($oneOfFields = ["position", "moveEvent", "spawnEvent", "putBombEvent", "destroyBoxEvent"]),
        set: $util.oneOfSetter($oneOfFields)
    });

    /**
     * Creates a new Event instance using the specified properties.
     * @function create
     * @memberof Event
     * @static
     * @param {IEvent=} [properties] Properties to set
     * @returns {Event} Event instance
     */
    Event.create = function create(properties) {
        return new Event(properties);
    };

    /**
     * Encodes the specified Event message. Does not implicitly {@link Event.verify|verify} messages.
     * @function encode
     * @memberof Event
     * @static
     * @param {IEvent} message Event message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    Event.encode = function encode(message, writer) {
        if (!writer)
            writer = $Writer.create();
        writer.uint32(/* id 1, wireType 2 =*/10).string(message.eventName);
        if (message.position != null && message.hasOwnProperty("position"))
            $root.Position.encode(message.position, writer.uint32(/* id 2, wireType 2 =*/18).fork()).ldelim();
        if (message.moveEvent != null && message.hasOwnProperty("moveEvent"))
            $root.MoveEvent.encode(message.moveEvent, writer.uint32(/* id 3, wireType 2 =*/26).fork()).ldelim();
        if (message.spawnEvent != null && message.hasOwnProperty("spawnEvent"))
            $root.SpawnEvent.encode(message.spawnEvent, writer.uint32(/* id 4, wireType 2 =*/34).fork()).ldelim();
        if (message.putBombEvent != null && message.hasOwnProperty("putBombEvent"))
            $root.PubBombEvent.encode(message.putBombEvent, writer.uint32(/* id 5, wireType 2 =*/42).fork()).ldelim();
        if (message.destroyBoxEvent != null && message.hasOwnProperty("destroyBoxEvent"))
            $root.DestroyBoxEvent.encode(message.destroyBoxEvent, writer.uint32(/* id 6, wireType 2 =*/50).fork()).ldelim();
        return writer;
    };

    /**
     * Encodes the specified Event message, length delimited. Does not implicitly {@link Event.verify|verify} messages.
     * @function encodeDelimited
     * @memberof Event
     * @static
     * @param {IEvent} message Event message or plain object to encode
     * @param {$protobuf.Writer} [writer] Writer to encode to
     * @returns {$protobuf.Writer} Writer
     */
    Event.encodeDelimited = function encodeDelimited(message, writer) {
        return this.encode(message, writer).ldelim();
    };

    /**
     * Decodes an Event message from the specified reader or buffer.
     * @function decode
     * @memberof Event
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @param {number} [length] Message length if known beforehand
     * @returns {Event} Event
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    Event.decode = function decode(reader, length) {
        if (!(reader instanceof $Reader))
            reader = $Reader.create(reader);
        var end = length === undefined ? reader.len : reader.pos + length, message = new $root.Event();
        while (reader.pos < end) {
            var tag = reader.uint32();
            switch (tag >>> 3) {
            case 1:
                message.eventName = reader.string();
                break;
            case 2:
                message.position = $root.Position.decode(reader, reader.uint32());
                break;
            case 3:
                message.moveEvent = $root.MoveEvent.decode(reader, reader.uint32());
                break;
            case 4:
                message.spawnEvent = $root.SpawnEvent.decode(reader, reader.uint32());
                break;
            case 5:
                message.putBombEvent = $root.PubBombEvent.decode(reader, reader.uint32());
                break;
            case 6:
                message.destroyBoxEvent = $root.DestroyBoxEvent.decode(reader, reader.uint32());
                break;
            default:
                reader.skipType(tag & 7);
                break;
            }
        }
        if (!message.hasOwnProperty("eventName"))
            throw $util.ProtocolError("missing required 'eventName'", { instance: message });
        return message;
    };

    /**
     * Decodes an Event message from the specified reader or buffer, length delimited.
     * @function decodeDelimited
     * @memberof Event
     * @static
     * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
     * @returns {Event} Event
     * @throws {Error} If the payload is not a reader or valid buffer
     * @throws {$protobuf.util.ProtocolError} If required fields are missing
     */
    Event.decodeDelimited = function decodeDelimited(reader) {
        if (!(reader instanceof $Reader))
            reader = new $Reader(reader);
        return this.decode(reader, reader.uint32());
    };

    /**
     * Verifies an Event message.
     * @function verify
     * @memberof Event
     * @static
     * @param {Object.<string,*>} message Plain object to verify
     * @returns {string|null} `null` if valid, otherwise the reason why it is not
     */
    Event.verify = function verify(message) {
        if (typeof message !== "object" || message === null)
            return "object expected";
        var properties = {};
        if (!$util.isString(message.eventName))
            return "eventName: string expected";
        if (message.position != null && message.hasOwnProperty("position")) {
            properties.payload = 1;
            {
                var error = $root.Position.verify(message.position);
                if (error)
                    return "position." + error;
            }
        }
        if (message.moveEvent != null && message.hasOwnProperty("moveEvent")) {
            if (properties.payload === 1)
                return "payload: multiple values";
            properties.payload = 1;
            {
                var error = $root.MoveEvent.verify(message.moveEvent);
                if (error)
                    return "moveEvent." + error;
            }
        }
        if (message.spawnEvent != null && message.hasOwnProperty("spawnEvent")) {
            if (properties.payload === 1)
                return "payload: multiple values";
            properties.payload = 1;
            {
                var error = $root.SpawnEvent.verify(message.spawnEvent);
                if (error)
                    return "spawnEvent." + error;
            }
        }
        if (message.putBombEvent != null && message.hasOwnProperty("putBombEvent")) {
            if (properties.payload === 1)
                return "payload: multiple values";
            properties.payload = 1;
            {
                var error = $root.PubBombEvent.verify(message.putBombEvent);
                if (error)
                    return "putBombEvent." + error;
            }
        }
        if (message.destroyBoxEvent != null && message.hasOwnProperty("destroyBoxEvent")) {
            if (properties.payload === 1)
                return "payload: multiple values";
            properties.payload = 1;
            {
                var error = $root.DestroyBoxEvent.verify(message.destroyBoxEvent);
                if (error)
                    return "destroyBoxEvent." + error;
            }
        }
        return null;
    };

    /**
     * Creates an Event message from a plain object. Also converts values to their respective internal types.
     * @function fromObject
     * @memberof Event
     * @static
     * @param {Object.<string,*>} object Plain object
     * @returns {Event} Event
     */
    Event.fromObject = function fromObject(object) {
        if (object instanceof $root.Event)
            return object;
        var message = new $root.Event();
        if (object.eventName != null)
            message.eventName = String(object.eventName);
        if (object.position != null) {
            if (typeof object.position !== "object")
                throw TypeError(".Event.position: object expected");
            message.position = $root.Position.fromObject(object.position);
        }
        if (object.moveEvent != null) {
            if (typeof object.moveEvent !== "object")
                throw TypeError(".Event.moveEvent: object expected");
            message.moveEvent = $root.MoveEvent.fromObject(object.moveEvent);
        }
        if (object.spawnEvent != null) {
            if (typeof object.spawnEvent !== "object")
                throw TypeError(".Event.spawnEvent: object expected");
            message.spawnEvent = $root.SpawnEvent.fromObject(object.spawnEvent);
        }
        if (object.putBombEvent != null) {
            if (typeof object.putBombEvent !== "object")
                throw TypeError(".Event.putBombEvent: object expected");
            message.putBombEvent = $root.PubBombEvent.fromObject(object.putBombEvent);
        }
        if (object.destroyBoxEvent != null) {
            if (typeof object.destroyBoxEvent !== "object")
                throw TypeError(".Event.destroyBoxEvent: object expected");
            message.destroyBoxEvent = $root.DestroyBoxEvent.fromObject(object.destroyBoxEvent);
        }
        return message;
    };

    /**
     * Creates a plain object from an Event message. Also converts values to other types if specified.
     * @function toObject
     * @memberof Event
     * @static
     * @param {Event} message Event
     * @param {$protobuf.IConversionOptions} [options] Conversion options
     * @returns {Object.<string,*>} Plain object
     */
    Event.toObject = function toObject(message, options) {
        if (!options)
            options = {};
        var object = {};
        if (options.defaults)
            object.eventName = "";
        if (message.eventName != null && message.hasOwnProperty("eventName"))
            object.eventName = message.eventName;
        if (message.position != null && message.hasOwnProperty("position")) {
            object.position = $root.Position.toObject(message.position, options);
            if (options.oneofs)
                object.payload = "position";
        }
        if (message.moveEvent != null && message.hasOwnProperty("moveEvent")) {
            object.moveEvent = $root.MoveEvent.toObject(message.moveEvent, options);
            if (options.oneofs)
                object.payload = "moveEvent";
        }
        if (message.spawnEvent != null && message.hasOwnProperty("spawnEvent")) {
            object.spawnEvent = $root.SpawnEvent.toObject(message.spawnEvent, options);
            if (options.oneofs)
                object.payload = "spawnEvent";
        }
        if (message.putBombEvent != null && message.hasOwnProperty("putBombEvent")) {
            object.putBombEvent = $root.PubBombEvent.toObject(message.putBombEvent, options);
            if (options.oneofs)
                object.payload = "putBombEvent";
        }
        if (message.destroyBoxEvent != null && message.hasOwnProperty("destroyBoxEvent")) {
            object.destroyBoxEvent = $root.DestroyBoxEvent.toObject(message.destroyBoxEvent, options);
            if (options.oneofs)
                object.payload = "destroyBoxEvent";
        }
        return object;
    };

    /**
     * Converts this Event to JSON.
     * @function toJSON
     * @memberof Event
     * @instance
     * @returns {Object.<string,*>} JSON object
     */
    Event.prototype.toJSON = function toJSON() {
        return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
    };

    return Event;
})();

module.exports = $root;

import { ILobby } from '@/network/lobby';
import { Connection } from '@/network/connection';

import { Player } from './entities/player';
import { IEntity } from './entities/typedef';
import { ITerrain, Terrain, TerrainSchema } from './terrain';
import { IEngine, Position, EventName, EventPayload } from './typedef';

export type GameState = (IEntity | null)[][];

export class Engine implements IEngine {
  public terrain: ITerrain;

  constructor(public lobby: ILobby, terrainSchema: TerrainSchema) {
    this.terrain = new Terrain(this, terrainSchema);
  }

  public emitEvent(eventName: EventName, payload: EventPayload) {
    this.lobby.broadcast(eventName, payload);
  }

  public handleEvent(eventName: string, payload: any) {}

  private generatePosition(): Position {
    const spawnPoints = Object.entries(this.terrain.availableSpawnPoints);
    const randomIndex = Math.floor(Math.random() * spawnPoints.length);

    const [position] = spawnPoints[randomIndex];
    delete this.terrain.availableSpawnPoints[position];

    const [x, y] = position.split(':').map(Number);
    return { x, y };
  }

  public spawnPlayer(client: Connection): Player {
    const spawnPosition = this.generatePosition();
    const player = new Player(this, spawnPosition, client.id, client.nickname);

    this.lobby.players[client.id] = player;
    return player;
  }
}

import { IPlayer } from './entities/player';
import { ITerrain } from './terrain';
import { BoostKind } from './entities/boost';

export interface Position {
  x: number;
  y: number;
}

export interface MoveEvent {
  player: IPlayer;
  direction: 'up' | 'down' | 'left' | 'right';
}
export interface SpawnEvent {
  player: IPlayer;
  position: Position;
}
export type PutBombEvent = SpawnEvent;
export interface DestroyBoxEvent {
  position: Position;
  boostKind: BoostKind | null;
}

export type EventName =
  | 'start'
  | 'move'
  | 'spawn'
  | 'putBomb'
  | 'destroyBox'
  | 'bombExplode';
export type EventPayload =
  | Position
  | MoveEvent
  | SpawnEvent
  | PutBombEvent
  | DestroyBoxEvent
  | null;

export interface IEngine {
  terrain: ITerrain;

  emitEvent(eventName: EventName, payload: EventPayload): void;
}

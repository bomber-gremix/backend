import { Engine } from '@/engine';
import { ILobby } from '@/network/lobby';

import { Bomb } from './bomb';
import { Player } from './player';

describe('Bomb entity test', () => {
  it('Should explode boxes properly', () => {
    const engine = new Engine((null as unknown) as ILobby, [
      [null, null],
      [null, 'BOX'],
    ]);

    const emitEventMock = jest.fn();
    engine.emitEvent = emitEventMock;

    const mockPlayer = new Player(engine, { x: 1, y: 0 });
    mockPlayer.stats = { bombRadius: 1, maxHealth: 0, bombCount: 1 };

    const bomb = new Bomb(engine, mockPlayer);
    expect(bomb.radius).toBe(1);

    engine.terrain.map[1][0] = bomb;

    bomb.explode();

    expect(
      engine.terrain.map[1][1] === null ||
        engine.terrain.map[1][1]!.entityType === 'BOOST',
    ).toBeTruthy();
    expect(emitEventMock.mock.calls.length).toBeGreaterThanOrEqual(2);

    const calls = emitEventMock.mock.calls.reduce(
      (obj, [eventName, args]) => ({ ...obj, ...{ [eventName]: args } }),
      {},
    );

    expect(calls.destroyBox.position).toMatchObject({ x: 1, y: 1 });
    expect(calls.bombExplode).toMatchObject({ x: 1, y: 0 });
  });

  it('Should explode only 1 box in line', () => {
    const engine = new Engine((null as unknown) as ILobby, [
      [null, null, null],
      [null, 'BOX', 'BOX'],
      [null, null, null],
    ]);

    const emitEventMock = jest.fn();
    engine.emitEvent = emitEventMock;

    const mockPlayer = new Player(engine, { x: 1, y: 0 });
    mockPlayer.stats = { bombRadius: 2, maxHealth: 0, bombCount: 1 };

    const bomb = new Bomb(engine, mockPlayer);
    expect(bomb.radius).toBe(2);

    engine.terrain.map[1][0] = bomb;

    bomb.explode();

    expect(
      engine.terrain.map[1][1] === null ||
        engine.terrain.map[1][1]!.entityType === 'BOOST',
    ).toBeTruthy();
    expect(emitEventMock.mock.calls.length).toBeGreaterThanOrEqual(2);

    const calls = emitEventMock.mock.calls.reduce(
      (obj, [eventName, args]) => ({ ...obj, ...{ [eventName]: args } }),
      {},
    );

    expect(calls.destroyBox.position).toMatchObject({ x: 1, y: 1 });
    expect(calls.bombExplode).toMatchObject({ x: 1, y: 0 });
    if (calls.boostDrop) expect(calls.boostDrop).toMatchObject({ x: 1, y: 1 });

    expect(engine.terrain.map[1][2]!.entityType).toBe('BOX');
  });

  it('Should not explode blocks', () => {
    const engine = new Engine((null as unknown) as ILobby, [
      [null, null, null],
      [null, 'BLOCK', 'BOX'],
      [null, null, null],
    ]);

    const emitEventMock = jest.fn();
    engine.emitEvent = emitEventMock;

    const mockPlayer = new Player(engine, { x: 1, y: 0 });
    mockPlayer.stats = { bombRadius: 2, maxHealth: 0, bombCount: 1 };

    const bomb = new Bomb(engine, mockPlayer);
    expect(bomb.radius).toBe(2);

    engine.terrain.map[1][0] = bomb;

    bomb.explode();

    expect(engine.terrain.map[1][1]!.entityType).toBe('BLOCK');
    expect(emitEventMock).toBeCalledTimes(1);

    const calls = emitEventMock.mock.calls.reduce(
      (obj, [eventName, args]) => ({ ...obj, ...{ [eventName]: args } }),
      {},
    );
    expect(calls.bombExplode).toMatchObject({ x: 1, y: 0 });

    expect(engine.terrain.map[1][2]!.entityType).toBe('BOX');
  });

  it('Should explode players', () => {
    const engine = new Engine((null as unknown) as ILobby, [
      [null, null, null],
      [null, 'SPAWNPOINT', null],
      [null, null, null],
    ]);

    const emitEventMock = jest.fn();
    engine.emitEvent = emitEventMock;

    const player = new Player(engine, { x: 1, y: 0 }, '1');
    player.dealDamage = jest.fn(player.dealDamage);

    const bomb = new Bomb(engine, player);
    expect(bomb.radius).toBe(1);

    engine.terrain.map[1][0] = bomb;
    player.position.y = 1;
    engine.terrain.map[1][1] = player;

    bomb.explode();

    expect(engine.terrain.map[1][1]).toBe(null);
    expect(emitEventMock).toBeCalledTimes(1);
    expect(player.dealDamage).toBeCalledTimes(1);

    const [[event, payload]] = emitEventMock.mock.calls;
    expect(event).toBe('bombExplode');
    expect(payload).toMatchObject({ x: 1, y: 0 });
  });
});

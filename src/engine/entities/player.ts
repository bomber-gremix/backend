import { Boost } from './boost';
import { Bomb } from './bomb';
import { Engine } from '../index';
import { IEntity } from './typedef';
import { Position, EventName, EventPayload } from '../typedef';

export interface PlayerStats {
  maxHealth: number;
  bombCount: number;
  bombRadius: number;
}

export interface IPlayer extends IEntity {
  id: string;
  stats: PlayerStats;
  health: number;
  nickname: string;
  position: Position;

  putBomb(): void;
  dealDamage(): void;
  move(direction: 'up' | 'down' | 'left' | 'right'): void;
}

export class Player implements IPlayer {
  public entityType = 'PLAYER';

  public stats: PlayerStats = {
    maxHealth: 1,
    bombCount: 1,
    bombRadius: 1,
  };
  public health = 1;

  constructor(
    private engine: Engine,
    public position: Position,
    public id: string = '',
    public nickname: string = '',
  ) {}

  public emitEvent(eventName: EventName, payload: EventPayload) {
    this.engine.lobby.connections[this.id].send(eventName, payload);
  }
  private die() {
    this.engine.terrain.map[this.position.x][this.position.y] = null;
  }

  dealDamage() {
    this.health -= 1;
    if (this.health <= 0) this.die();
  }
  move(direction: 'up' | 'down' | 'left' | 'right'): void {
    const { x, y } = this.position;

    switch (direction) {
      case 'up':
        this.position.x -= 1;
        break;
      case 'down':
        this.position.x += 1;
        break;
      case 'right':
        this.position.y -= 1;
        break;
      case 'left':
        this.position.y += 1;
        break;
    }

    const { x: newX, y: newY } = this.position;
    const newCell = this.engine.terrain.map[newX][newY];

    if (newCell !== null && newCell.entityType !== 'BOOST') {
      this.position = { x, y };
      return;
    }

    this.engine.terrain.map[x][y] = null;
    if (newCell && newCell.entityType === 'BOOST') {
      (newCell as Boost).consume(this);
    }
    this.engine.terrain.map[newX][newY] = this;
  }
  putBomb(): void {
    const { x, y } = this.position;
    this.engine.terrain.map[x][y] = new Bomb(this.engine, this);
    this.engine.emitEvent('putBomb', { player: this, position: this.position });
  }
}

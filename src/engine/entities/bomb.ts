import { IPlayer } from './player';
import { IEntity } from './typedef';
import { IEngine, Position } from '../typedef';

import { Box } from './box';

let visit: boolean[][] | null = null;
export class Bomb implements IEntity {
  public entityType = 'BOMB';

  private position: Position;
  public radius: number;

  constructor(private engine: IEngine, private player: IPlayer) {
    this.radius = this.player.stats.bombRadius;
    this.position = { ...this.player.position };
  }

  private static directions = [[1, 0], [-1, 0], [0, 1], [0, -1]];

  public explode(
    x: number = this.position.x,
    y: number = this.position.y,
    d: number = 0,
    delta: number[] | null = null,
  ) {
    if (d > this.radius) return;
    if (visit == null) {
      visit = Array(this.engine.terrain.size)
        .fill(null)
        .map(() => Array(this.engine.terrain.size).fill(false));
    }
    if (visit[x][y]) return;
    visit[x][y] = true;

    let shouldSpread = true;
    const entity: IEntity | null = this.engine.terrain.map[x][y];

    if (entity) {
      if (entity.entityType === 'BLOCK' || entity.entityType === 'BOX') {
        shouldSpread = false;
      }

      switch (entity.entityType) {
        case 'BOX':
          (entity as Box).destroy();
          break;
        case 'PLAYER':
          (entity as IPlayer).dealDamage();
          break;
        case 'BOMB':
          if (d > 0) {
            (this.engine.terrain.map[x][y] as Bomb).explode();
          } else {
            const { x: playerX, y: playerY } = this.player.position;
            if (playerX === x && playerY === y) this.player.dealDamage();
            this.engine.emitEvent('bombExplode', this.position);
          }
          this.engine.terrain.map[x][y] = null;
          break;
      }
    }

    if (!shouldSpread) return;

    const directions = delta === null ? Bomb.directions : [delta];
    for (const delta of directions) {
      const dX = x + delta[0];
      const dY = y + delta[1];

      if (dX < 0 || dX >= this.engine.terrain.size) continue;
      if (dY < 0 || dY >= this.engine.terrain.size) continue;

      this.explode(dX, dY, d + 1, delta);
    }

    if (d === 0) visit = null;
  }

  public delay() {
    setTimeout(this.explode, 3000);
  }
}

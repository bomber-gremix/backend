import { IPlayer } from './player';
import { IEntity } from './typedef';

export type BoostKind = 'bombRadius' | 'bombCount' | 'health';
const boostKinds: BoostKind[] = ['bombRadius', 'bombCount', 'health'];

export class Boost implements IEntity {
  public entityType = 'BOOST';
  public kind: BoostKind;

  constructor() {
    this.kind = boostKinds[Math.floor(Math.random() * boostKinds.length)];
  }

  public consume(player: IPlayer) {
    switch (this.kind) {
      case 'bombRadius':
        player.stats.bombRadius += 1;
        break;
      case 'bombCount':
        player.stats.bombCount += 1;
        break;
      case 'health':
        if (player.health < player.stats.maxHealth) player.health += 1;
        else player.stats.maxHealth += 1;
        break;
    }
  }
}

import { IEntity } from './typedef';
import { IEngine, Position } from '../typedef';

import { Boost } from './boost';

export class Box implements IEntity {
  public entityType = 'BOX';

  constructor(private engine: IEngine, private position: Position) {}

  public destroy() {
    const shouldSpawnBoost = Math.random() > 0.2;
    const boost = shouldSpawnBoost ? new Boost() : null;
    this.engine.terrain.map[this.position.x][this.position.y] = boost;
    this.engine.emitEvent('destroyBox', {
      position: this.position,
      boostKind: boost ? boost.kind : null,
    });
  }
}

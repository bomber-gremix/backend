import { Boost } from './boost';
import { Player } from './player';

describe('Boost entity test', () => {
  it('Should generate random boost', () => {
    const boost = new Boost();
    expect(
      ['bombRadius', 'bombCount', 'health'].includes(boost.kind),
    ).toBeTruthy();
  });
  it('Should consume boost properly', () => {
    const boost = new Boost();

    boost.kind = 'bombCount';

    const player = new Player(null as any, { x: 0, y: 0 });
    player.stats = { bombRadius: 0, maxHealth: 1, bombCount: 0 };
    boost.consume(player);

    expect(player.stats.bombCount).toBe(1);

    boost.kind = 'bombRadius';
    boost.consume(player);
    expect(player.stats.bombRadius).toBe(1);

    boost.kind = 'health';
    boost.consume(player);
    expect(player.stats.maxHealth).toBe(2);

    boost.kind = 'health';
    boost.consume(player);
    expect(player.health).toBe(2);
  });
});

import { Box } from './entities/box';
import { Block } from './entities/block';
import { Engine } from './index';
import { IEntity } from './entities/typedef';
import { Position } from './typedef';

export type TerrainCell = 'SPAWNPOINT' | 'BOX' | 'BLOCK' | null;
export type TerrainSchema = TerrainCell[][];

export interface ITerrain {
  map: (IEntity | null)[][];
  size: number;
  zone: Position[];
  availableSpawnPoints: { [key: string]: boolean };

  isInSafeZone(position: Position): boolean;
}

export class Terrain implements ITerrain {
  map: (IEntity | null)[][];
  size: number;
  zone: Position[] = [{ x: 0, y: 0 }];
  availableSpawnPoints: { [key: string]: boolean } = {};

  constructor(engine: Engine, schema: TerrainSchema) {
    this.size = schema.length;
    this.zone.push({ x: this.size - 1, y: this.size - 1 });

    this.map = Array(this.size)
      .fill(null)
      .map((_, i) =>
        Array(this.size)
          .fill(null)
          .map((_, j) => {
            switch (schema[i][j]) {
              case 'SPAWNPOINT':
                this.availableSpawnPoints[`${i}:${j}`] = true;
                return null;
              case 'BLOCK':
                return new Block();
              case 'BOX':
                return new Box(engine, { x: i, y: j });
              default:
                return null;
            }
          }),
      );
  }

  public isInSafeZone(position: Position) {
    return (
      position.x >= this.zone[0].x &&
      position.x <= this.zone[1].x &&
      position.y >= this.zone[0].y &&
      position.y <= this.zone[1].y
    );
  }
}
